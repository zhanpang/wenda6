package com.company;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    /**
     * 打印
     * @param index  索引
     * @param object
     */
    public  static void print(int index,Object object){
        System.out.println(String.format("{%d},%s",index,object.toString()));
    }
    public static void demolist(){
        List<String> strlist=new ArrayList<String>(10);
        for(int i=0;i<4;i++){
            strlist.add(String.valueOf(i*i));
        }
        List<String> strListB=new ArrayList<String >(10);
        for(int i=0;i<4;i++){
            strListB.add(String.valueOf(i));
        }
        strlist.addAll(strListB);
        strlist.remove(String.valueOf(1));
        Collections.reverse(strlist);
        Collections.sort(strlist);
        Collections.sort(strlist, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2  .compareTo(o1);
            }
        });
        print(1,strlist);
        for(String a:strlist){
            print(2,a);
        }
        int []array=new int[]{1,2,3,4,5};
        print(11,1);
    }
    public static void demoMapTable(){
        Map<String,String> map = new HashMap<String, String>();
        for(int i=0 ;i < 10 ;i    ++){
            map.put(String.valueOf(i),String.valueOf(i*i));

        }
        for( Map.Entry< String,String> entry : map.entrySet()) {
            print (2,entry.getKey() +" | "+entry.getValue());

        }
        print(3,map.values());
        print(4,map.keySet());
        print(5,map.get("3"));
        print(777,333);
        print(6,map.containsKey("A"));
        map.replace("3","27");

        print(7,map.get("3"));
    }
    public static void demoOperation(){
        print(1,5+2);
        print(2,5-2);
        print(3,5*2);
        print(4,5.0/2);
        print(5,5%2);
        print(6,5<<2);
        print(7,5>>2);
        print(8,5|2);
        print(9,5^2);
        print(10,5==2);
        print(11,5!=2);
        int a=11;
        double b=2.2f;


    }
    public static void demoPush_git(){
        print(3,4);
    }
    public static void demoException() {
        try{
            int k=3;
            // k/=0;
            if(k==3){
                throw new Exception("詹乓故意的");
            }
        }catch(Exception e){
            print(2,e.getMessage());
        }finally {
            print(3,"finally");
        }
    }
    public static void demoString(){
        String str="hello world";
        print(1,str.indexOf('e'));
        print(2,str.charAt(3));
        print(3,str.codePointAt(1));
        print(4,str.compareToIgnoreCase("HELLO WORLD"));

    }
    public static void demoFunction(){
        Random random = new Random();
        random.setSeed(1);
        print(1,random.nextInt(1000));
        print(2,random.nextFloat());
        List<Integer> array = Arrays.asList(new Integer[]{1,2,3,4,5});
        Collections.shuffle(array);
        print(3,array);
        Date date = new Date();
        print(4,date.getTime());
        print(5,date);
        DateFormat df = new SimpleDateFormat("yyyy/MM-dd HH:mm:ss");
        print(6,df.format(date));
        print(7,UUID.randomUUID());
        print(9,Math.log(10));
        print(10,Math.min(323,45));
        print(11,Math.ceil(2.2));
        print(12,Math.floor(2.2));
    }
    public static void main(String[] args) {

       /*System.out.println(1+" hello");
        print(1,"Hello world");
        demoOperation();
       */
        //demoString();
        //demolist();
        //demoMapTable();
        // demoException();
        // say();
        demoFunction();
    }
}
